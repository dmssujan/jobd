<?php

namespace App\Http\Controllers;


use App\Brand;
use App\Category;
use App\Product;
use Illuminate\Http\Request;
use DB;
use Image;
class ProductController extends Controller
{
    public function index(){
      $categories = Category::where('publication_status', 1)->get();
      $brands     = Brand::where('publication_status', 1)->get();
      return view('admin.product.add-product', [
        'categories' => $categories,
        'brands' => $brands,
      ]);
    }

    protected function infoValidation($request){
     $this->validate($request, [
       'category_id' =>'required',
       'brand_id' =>'required',
       'product_name' =>'required',
       'product_price' =>'required',
       'product_quantity' =>'required',
       'short_description' =>'required',
       'long_description' =>'required',
       'product_image' =>'required',
       'publication_status' =>'required'
     ]);
    }
    protected function imageUpload($request){

      // $productImage= $request -> file('product_image');
      // $imageName = $productImage -> getClientOriginalName();
      // $directory = 'product-images/';
      // $imageUrl = $directory.$imageName;
      // Image::make($productImage)->save($imageUrl);
      // return $imageUrl;
      //test bind_textdomain_codeset
      $productImage= $request -> file('product_image');
      $fileType = $productImage -> getClientOriginalExtension();
      $imageName = $request-> product_name.'.'.$fileType;
      $directory = 'product-images/';
      $imageUrl = $directory.$imageName;
      Image::make($productImage)->resize(200,200)->save($imageUrl);
      return $imageUrl;
    //  $productImage ->move($directory, $imageName);

    }
   protected function basicSave($request,$imageUrl){
     $product = new Product();
     $product -> category_id = $request -> category_id;
     $product -> brand_id = $request -> brand_id;
     $product -> product_name = $request -> product_name;
     $product -> product_price = $request -> product_price;
     $product -> product_quantity = $request -> product_quantity;
     $product -> short_description = $request -> short_description;
     $product -> long_description = $request -> long_description;
     $product -> product_image = $imageUrl;
     $product -> publication_status = $request -> publication_status;
     $product -> save();
   }

    public function saveProduct(Request $request){
      $this->infoValidation($request);
      $imageUrl = $this->imageUpload($request);
      $this ->basicSave($request,$imageUrl);


      return redirect('/product/add')->with('message', 'Product info save successfully');


    }
    public function manageProduct(){
      $products = DB::table('products')
                  ->join('categories', 'products.category_id', '=', 'categories.id')
                  ->join('brands', 'products.brand_id', '=', 'brands.id')
                  ->select('products.*', 'categories.category_name','brands.brand_name')
                  ->get();
      return view('admin.product.manage-product',['products' => $products]);
    }
    public function unpublishProduct($id){
      $product = Product::find($id);
      $product -> publication_status = 0;
      $product -> save();
      return redirect('/product/manage')->with('message', 'Product Unpublished successfully');
    }

    public function publishProduct($id){
      $product = Product::find($id);
      $product -> publication_status = 1;
      $product -> save();
      return redirect('/product/manage')->with('message', 'Product Published successfully');
    }
    // public function editProduct($id){
    //    $product = Product::find($id);
    //    $categories = Category::where('publication_status', 1)->get();
    //    $brands= Brand::where('publication_status', 1)->get();
    //    return view('admin.product.edit-product', [
    //      'product'=>$product,
    //      'categories'=>$categories,
    //      'brands'=>$brands
    //    ]);
    // }
    public function editProduct($id){
      $product = Product::find($id);
      $categories = Category::where('publication_status', 1)->get();
      $brands     = Brand::where('publication_status', 1)->get();
      return view('admin.product.edit-product', [
        'product' => $product,
        'categories' => $categories,
        'brands' => $brands,
      ]);

    }


    public function deleteProduct($id){
      $product = Product::find($id);
      $product-> delete();
      return redirect('/product/manage')->with('message', 'Product Delete Successfully');
    }

    public function productBsicInfo($request,$product,$imageUrl=null){
      $product -> category_id = $request -> category_id;
      $product -> brand_id = $request -> brand_id;
      $product -> product_name = $request -> product_name;
      $product -> product_price = $request -> product_price;
      $product -> product_quantity = $request -> product_quantity;
      $product -> short_description = $request -> short_description;
      $product -> long_description = $request -> long_description;
      if($imageUrl){
        $product -> product_image = $imageUrl;
      }

      $product -> publication_status = $request -> publication_status;
      $product -> save();
    }
    public function updateProduct(Request $request){

     $productImage = $request->file('product_image');
     $product = Product::find($request->product_id);
     if($productImage){

      unlink($product->product_image);

      $imageUrl = $this->imageUpload($request);

      $this -> productBsicInfo($request,$product,$imageUrl);

     }else{
       $this -> productBsicInfo($request,$product);
    }
    return redirect('/product/manage')->with('message','Product Info Update Successfully');
  }
}

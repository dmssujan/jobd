<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
class NewShopController extends Controller
{
    public function index(){
     $categories = Category::where('publication_status', 1)->get();
     $newProducts = Product::where('publication_status', 1)
                           ->orderBy('id','DESC')
                           ->take(3)
                           ->get();

     return view('front-end.home.home', [
       'categories' =>$categories,
       'newProducts' =>$newProducts
     ]);
    }
    public function home2(){
      $newProducts = Product::where('publication_status', 1)
                            ->orderBy('id','DESC')
                            ->take(5)
                            ->get();
      return view('front-end.home.home2', [
        'newProducts' =>$newProducts
      ]);
    }
    public function categoryProduct($id){

    $categoryProducts = Product::where('category_id', $id)
                     ->  where('publication_status', 1)
                     -> get();

      return view('front-end.categories.category-content', [
        'categoryProducts' => $categoryProducts
      ]);
    }

    public function productDetails($id){
      $product = Product::find($id);
      return view('front-end.products.product-details', [
        'product' => $product
      ]);
    }
    public function search(Request $request){
      $query = $request ->input('query');
     $seProducts = Product::where('product_name', 'Like', '%$query%')->get();
      return view('front-end.home.search',[
        'seProducts' => $seProducts
      ]);
    }
}

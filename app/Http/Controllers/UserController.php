<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function manageUser(){
      $users = User::paginate(5);
      return view('admin.users.manage-user', ['users' => $users]);
    }

    public function index(){

      return view('admin.users.add-user');
    }

    public function logout(){
      auth()->logout();
      session()->flash('message', 'Some goodbye message');
     return redirect('/login');
    }
}

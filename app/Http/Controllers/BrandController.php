<?php

namespace App\Http\Controllers;
use App\Brand;
use Illuminate\Http\Request;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.brand.add-brand');
    }
    protected function brandValidation($request){
      $this-> validate($request, [
        'brand_name' => 'required|regex:/^[\pL\s\-]+$/u|max:25|min:3',
        'brand_description' => 'required',
        'publication_status' => 'required'

      ]);
    }
    public function saveBrand(Request $request){
     $this->brandValidation($request);
      $brand  = new Brand();
      $brand -> brand_name = $request -> brand_name;
      $brand -> brand_description = $request -> brand_description;
      $brand -> publication_status = $request -> publication_status;
      $brand ->save();
      return redirect('brand/add')->with('message', 'Brand Info save successfully');
    }

    public function manageBrand(){
      //$brands = Brand::all();
      $brands = Brand::paginate(5);
      return view('admin.brand.manage-brand', ['brands' => $brands]);
    }

    public function unpublishBrand($id){
     $brand = Brand::find($id);
     $brand ->publication_status = 0;
     $brand -> save();
     return redirect('brand/manage')->with('message','Brand unpublished Successfully');
    }

    public function publishBrand($id){
      $brand = Brand::find($id);
      $brand ->publication_status = 1;
      $brand -> save();
      return redirect('brand/manage')->with('message','Brand unpublished Successfully');
    }

    public function editBrand($id){
      $brand = Brand::find($id);
      return view('admin.brand.edit-brand',['brand' =>$brand]);
    }
    public function updateBrand(Request $request){
      $brand = Brand::find($request -> brand_id);
      $brand -> brand_name = $request -> brand_name;
      $brand -> brand_description = $request -> brand_description;
      $brand -> publication_status = $request -> publication_status;
      $brand -> save();
      return redirect('/brand/manage') -> with('message', 'Brand info update');
    }
    public function deleteBrand($id){
      $brand = Brand::find($id);
      $brand -> delete();
      return redirect('/brand/manage')->with('message', 'Brand Delete Successfully');
    }

}

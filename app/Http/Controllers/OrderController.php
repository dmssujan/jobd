<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Customer;
use App\Order;
use App\Shipping;
use App\Payment;
use App\OrderDetails;
use PDF;

class OrderController extends Controller
{
    public function manageOrderInfo(){
      $orders = DB::table('orders')
      ->join('customers', 'orders.customer_id', '=', 'customers.id')
      ->join('payments', 'orders.id', '=', 'payments.order_id')
      ->select('orders.*', 'customers.first_name','customers.last_name', 'payments.payment_type', 'payments.payment_status')
      ->get();



      return view('admin.order.manage-order',['orders' =>$orders]);
    }

    public function viewOrderDetails($id){
      $order = Order::find($id);
      $customer = Customer::find($order->customer_id);
      $shipping = Shipping::find($order->shipping_id);
      $payment = Payment::where('order_id', $order->id)->first();

      $orderDetails = OrderDetails::where('order_id', $order->id)->get();
      return view('admin.order.view-order', [
        'customer' => $customer,
        'order' => $order,
        'shipping' => $shipping,
        'payment' => $payment,
        'orderDetails' => $orderDetails
      ]);
    }

    public function viewOrderInvoice($id){
      $order = Order::find($id);
      $customer = Customer::find($order->customer_id);
      $shipping = Shipping::find($order->shipping_id);
    //  $payment = Payment::where('order_id', $order->id)->first();

      $orderDetails = OrderDetails::where('order_id', $order->id)->get();
      return view('admin.order.view-order-invoice', [
        'customer' => $customer,
        'order' => $order,
        'shipping' => $shipping,
      //  'payment' => $payment,
        'orderDetails' => $orderDetails
      ]);
    }

    public function downloadOrderInvoice($id){

      $order = Order::find($id);
      $customer = Customer::find($order->customer_id);
      $shipping = Shipping::find($order->shipping_id);
      $orderDetails = OrderDetails::where('order_id', $order->id)->get();

      $pdf = PDF::loadView('admin.order.downloa-invoice', [
        'customer' => $customer,
        'order' => $order,
        'shipping' => $shipping,
        'orderDetails' => $orderDetails
      ]);
      return $pdf->download('invoice.pdf');
    }

    public function editOrder($id){
      // $order = Order::find($id);
      // $customer = Customer::find($order->customer_id);
      // $shipping = Shipping::find($order->shipping_id);
      // $payment = Payment::where('order_id', $order->id)->first();
      //
      // $orderDetails = OrderDetails::where('order_id', $order->id)->get();
      // return view('admin.order.edit-order', [
      //   'customer' => $customer,
      //   'order' => $order,
      //   'shipping' => $shipping,
      //   'payment' => $payment,
      //   'orderDetails' => $orderDetails
      // ]);
    }

    public function updateOrder(Request $request){

      // $order = Order::find($id);
      // $customer = Customer::find($order->customer_id);
      // $shipping = Shipping::find($order->shipping_id);
      // $payment = Payment::where('order_id', $order->id)->first();
      //
      // $order -> first_name = $request -> first_name;
      // $order -> last_name = $request -> last_name;
      // $order -> order_status = $request -> order_status;
      // $order -> payment_status = $request -> payment_status;
      // $order -> save();
    }
}

@extends('admin.master')

@section('body')
<div class="row">
   <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default">
          <div class="panel-heading">
            <h4 class="text-center text-success">Manage User</h4>
          </div>

        </div>
        <div class="panel-body">
        <h3 class="text-center text-success" id="delete">{{ Session::get('message') }}</h3>
        <table class="table table-bordered table-striped">
          <tr>
            <th>SL No</th>
            <th>User id</th>
            <th>User Name</th>
            <th>User Email</th>
            <th>Action</th>
          </tr>
          @php
            $i = 1;
          @endphp
          @foreach ($users as $user)
          <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $user -> id }}</td>
            <td>{{ $user -> name}}</td>
            <td>{{ $user -> email}}</td>
            <td>
               <a href="#" class="btn btn-success">
                 <span class="glyphicon glyphicon-edit"></span>
               </a>
               <a href="#" class="btn btn-danger">
                 <span class="glyphicon glyphicon-trash"></span>
               </a>
            </td>

          </tr>
        @endforeach
        </table>
        {{ $users -> links()}}



      </div>
   </div>
</div>

@endsection

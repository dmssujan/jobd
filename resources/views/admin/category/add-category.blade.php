@extends('admin.master')

@section('body')
<div class="row">
   <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default">
        <h3 class="text-center text-success">{{ Session::get('message')}}</h3>
          <div class="panel-heading">
            <h4 class="text-center text-success">Add Category Form</h4>
          </div>

        </div>
        <div class="panel-body">
      <form action="{{ route('new-category') }}" class="form-horizontal" method="POST">
         @csrf
        <div class="form-group">
          <label class="control-label col-md-4">Category Name</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="category_name" class="form-control" placeholder="Category Name">
              <span class="text-danger">{{ $errors -> has('category_name') ? $errors -> first('category_name') : ' ' }}</span>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Category Description</label>
          <div class="col-md-8">
            <textarea class="form-control" name="category_description"></textarea>
             <span class="text-danger">{{ $errors -> has('category_description') ? $errors -> first('category_description') : ' ' }}</span>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Publication Status</label>
          <div class="col-md-8 radio">
            <label> <input type="radio" checked name="publication_status" value="1">Published </label>
            <label> <input type="radio"  name="publication_status" value="0"> Unpublished</label>

          </div>
        </div>
        <div class="form-group">

          <div class="col-md-8 col-md-offset-4">
            <input type="submit" class="btn btn-success" name="btn" value="save info">
          </div>
        </div>
      </form>
      </div>
   </div>
</div>

@endsection

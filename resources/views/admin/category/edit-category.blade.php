@extends('admin.master')

@section('body')
<div class="row">
   <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default">
        <h3 class="text-center text-success">{{ Session::get('message')}}</h3>
          <div class="panel-heading">
            <h4 class="text-center text-success">Add Category Form</h4>
          </div>

        </div>
        <div class="panel-body">
      <form action="{{ route('update-category') }}" class="form-horizontal" method="POST">
         @csrf
        <div class="form-group">
          <label class="control-label col-md-4">Category Name</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="category_name" value="{{ $category ->category_name }}" class="form-control" placeholder="Category Name">
            <input type="hidden" class="form-control" name="category_id" value="{{ $category ->id }}" class="form-control">
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Category Description</label>
          <div class="col-md-8">
            <textarea class="form-control" name="category_description">{{ $category ->category_description }}</textarea>

          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Publication Status</label>
          <div class="col-md-8 radio">
            <label> <input type="radio" checked name="publication_status" {{ $category->publication_status == 1 ? 'checked' : ' '}} value="1">Published </label>
            <label> <input type="radio"  name="publication_status" {{ $category->publication_status == 0 ? 'checked' : ' '}} value="0"> Unpublished</label>

          </div>
        </div>
        <div class="form-group">

          <div class="col-md-8 col-md-offset-4">
            <input type="submit" class="btn btn-success" name="btn" value="update info">
          </div>
        </div>
      </form>
      </div>
   </div>
</div>

@endsection

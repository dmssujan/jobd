<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<title>SB Admin 2 - Bootstrap Admin Theme</title>

<!-- Bootstrap Core CSS -->
<link href="{{ asset('/') }}back-end/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- MetisMenu CSS -->
<link href="{{ asset('/') }}back-end/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

<!-- Custom CSS -->
<link href="{{ asset('/') }}back-end/dist/css/sb-admin-2.css" rel="stylesheet">
<link href="{{ asset('/') }}back-end/dist/css/style.css" rel="stylesheet">

<!-- Morris Charts CSS -->
<link href="{{ asset('/') }}back-end/vendor/morrisjs/morris.css" rel="stylesheet">

<!-- Custom Fonts -->
<link href="{{ asset('/') }}back-end/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<script src="{{ asset('/') }}back-end/ckeditor/ckeditor.js"></script>
<script src="{{ asset('/') }}back-end/ckeditor/samples/js/sample.js"></script>
<link rel="stylesheet" href="{{ asset('/') }}back-end/ckeditor/samples/css/samples.css">
<link rel="stylesheet" href="{{ asset('/') }}back-end/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body>

<div id="wrapper">

<!-- Navigation -->

@include('admin.include.header')
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Dashboard</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
@yield('body')
</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

@include('admin.include.footer')

</body>

</html>

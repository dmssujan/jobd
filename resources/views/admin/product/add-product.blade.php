@extends('admin.master')

@section('body')
<div class="row">
   <div class="col-md-10 col-md-offset-0">

        <div class="panel panel-default">
        <h3 class="text-center text-success">{{ Session::get('message')}}</h3>
          <div class="panel-heading">
            <h4 class="text-center text-success">{{ 'Add Product Form' }}</h4>
          </div>

        </div>
        <div class="panel-body">
      <form action="{{ route('new-product') }}" class="form-horizontal" method="POST" enctype="multipart/form-data" >
         @csrf
        <div class="form-group">
          <label class="control-label col-md-4">Category Name</label>
          <div class="col-md-8">
             <select class="form-control" name="category_id">
               <option value="">-- Select Category --</option>
               @foreach ($categories as $category)
                 <option value="{{ $category->id }}">{{ $category->category_name }}</option>
               @endforeach

             </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Brand Name</label>
          <div class="col-md-8">
             <select class="form-control" name="brand_id">
               <option value="">-- Select Brand --</option>
              @foreach ($brands as $brand)
                <option value="{{ $brand ->id }}">{{ $brand ->brand_name }}</option>
              @endforeach

             </select>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Product Name</label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="product_name" />
            <span class="text-danger">{{ $errors -> has('product_name') ? $errors -> first('product_name') : ' ' }}</span>

          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Product Price</label>
          <div class="col-md-8">
            <input type="number" class="form-control" name="product_price" />
            <span class="text-danger">{{ $errors -> has('product_price') ? $errors -> first('product_price') : ' ' }}</span>

          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Product Quantity</label>
          <div class="col-md-8">
            <input type="number" class="form-control"  name="product_quantity" />
            <span class="text-danger">{{ $errors -> has('product_quantity') ? $errors -> first('product_quantity') : ' ' }}</span>

          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Product Short Description</label>
          <div class="col-md-8">
            <textarea class="form-control"  name="short_description"></textarea>
            <span class="text-danger">{{ $errors -> has('short_description') ? $errors -> first('short_description') : ' ' }}</span>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Product Long Description</label>
          <div class="col-md-8">
            <textarea class="form-control" id="editor" name="long_description"></textarea>
            <span class="text-danger">{{ $errors -> has('long_description') ? $errors -> first('long_description') : ' ' }}</span>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Product Image</label>
          <div class="col-md-8">
            <input type="file"  name="product_image"  accept="image/*"/>

          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Publication Status</label>
          <div class="col-md-8 radio">
            <label> <input type="radio" checked name="publication_status" value="1">Published </label>
            <label> <input type="radio"  name="publication_status" value="0"> Unpublished</label>

          </div>
        </div>
        <div class="form-group">

          <div class="col-md-8 col-md-offset-4">
            <input type="submit" class="btn btn-success" name="btn" value="save product info">
          </div>
        </div>
      </form>
      </div>
   </div>
</div>

@endsection

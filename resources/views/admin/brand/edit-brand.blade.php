@extends('admin.master')

@section('body')
<div class="row">
   <div class="col-md-8 col-md-offset-2">

        <div class="panel panel-default">
        <h3 class="text-center text-success">{{ Session::get('message')}}</h3>
          <div class="panel-heading">
            <h4 class="text-center text-success">Update Brand Form</h4>
          </div>

        </div>
        <div class="panel-body">
            {{ Form::open(['route'=>'update-brand', 'method'=>'POST', 'class'=>'form-horizontal']) }}
             <div class="form-group">
              {{ Form::label('brand_name', 'Brand Name', ['class'=>'control-label col-md-3']) }}
              <div class="col-md-9">

                @php($brandName = $brand ->brand_name)
               {{ Form::text('brand_name',$brandName,['class' => 'form-control']) }}

               {{-- <input type="text" class="form-control" name="brand_name" value="{{ $brand ->brand_name }}" class="form-control" placeholder="Category Name"> --}}
               <input type="hidden" class="form-control" name="brand_id" value="{{ $brand ->id }}" class="form-control">

               <span class="text-danger">{{ $errors -> has('brand_name') ? $errors -> first('brand_name') : ' ' }}</span>
              </div>
             </div>
             <div class="form-group">
              {{ Form::label('brand_description', 'Brand Description', ['class'=>'control-label col-md-3']) }}
              <div class="col-md-9">
                @php($brandDesc = $brand ->brand_description)
               {{ Form::text('brand_description',$brandDesc,['class' => 'form-control']) }}
                <span class="text-danger">{{ $errors -> has('brand_description') ? $errors -> first('brand_description') : ' ' }}</span>
              </div>
             </div>
             <div class="form-group">
              <label class="control-label col-md-3">Publication Status</label>
                <div class="col-md-9 radio">
                  <label><input type="radio" name="publication_status" {{$brand ->publication_status == 1 ? 'checked': ' '}} value="1"> Published</label>
                  <label><input type="radio" name="publication_status" {{$brand ->publication_status == 0 ? 'checked': ' '}} value="0"> Unpublished</label><br>
                    <span class="text-danger">{{ $errors -> has('publication_status') ? $errors -> first('publication_status') : ' ' }}</span>

                </div>
             </div>
             <div class="form-group">
              <div class="col-md-9 col-md-offset-3">
               <input type="submit" name="btn" value="update" class="btn btn-success">
              </div>
             </div>

            {{ Form::close() }}
      </div>
   </div>
</div>

@endsection

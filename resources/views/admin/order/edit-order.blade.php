@extends('admin.master')

@section('body')
<div class="row">
   <div class="col-md-10 col-md-offset-0">

        <div class="panel panel-default">
        <h3 class="text-center text-success">{{ Session::get('message')}}</h3>
          <div class="panel-heading">
            <h4 class="text-center text-success">{{ 'Edit Order' }}</h4>
          </div>

        </div>
        <div class="panel-body">
      <form action="{{ route('update-order') }}" class="form-horizontal" method="POST" enctype="multipart/form-data",  name="editProductForm" >
         @csrf

        <div class="form-group">
          <label class="control-label col-md-4">First Name</label>
          <div class="col-md-8">
            <input type="text" value="{{$customer->first_name}}" class="form-control" name="first_name" />
            <input type="hidden" value="{{ $customer->id}}" class="form-control" name="customer_id" />
            <span class="text-danger">{{ $errors -> has('first_name') ? $errors -> first('first_name') : ' ' }}</span>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-md-4">Last Name</label>
          <div class="col-md-8">
            <input type="text" value="{{$customer->last_name}}" class="form-control" name="last_name" />
            <input type="hidden" value="{{ $customer->id}}" class="form-control" name="customer_id" />
            <span class="text-danger">{{ $errors -> has('last_name') ? $errors -> first('last_name') : ' ' }}</span>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4">Order Status</label>
          <div class="col-md-8">
            <input type="text" value="{{$order->order_status}}" class="form-control" name="order_status" />
            <input type="hidden" value="{{ $order->id}}" class="form-control" name="order_id" />
            <span class="text-danger">{{ $errors -> has('order_status') ? $errors -> first('order_status') : ' ' }}</span>
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-md-4">Payment Status</label>
          <div class="col-md-8">
            <input type="text" value="{{$payment ->payment_status}}" class="form-control" name="payment_status" />
            <input type="hidden" value="{{ $payment->id}}" class="form-control" name="order_id" />
            <span class="text-danger">{{ $errors -> has('order_status') ? $errors -> first('order_status') : ' ' }}</span>
          </div>
        </div>



          <div class="form-group">

            <div class="col-md-8 col-md-offset-4">
              <input type="submit" class="btn btn-success" name="btn" value="update order info">
            </div>
          </div>
      </form>
      </div>
   </div>
</div>
@endsection

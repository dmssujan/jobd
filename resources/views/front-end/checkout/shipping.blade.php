@extends('front-end.master')
@section('body')
  <!-- ============================================
    Search box and main body start
  ============================================ -->
  <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="message">
                <h5 class="text-center">Dear {{ Session::get('customerName') }}. You have to give us product shipping info to complete your valiauable order. If your billing info & shipping info are same than just press on save shipping info button.</h5>
              </div>
            </div>

            <div class="col-md-6 col-md-offset-3">

                {{Form::open(['route'=>'new-shipping','method'=>'POST', 'class'=>'form-open'])}}
                 <h3>Shipping information goes here...</h3>
                 <hr>
                    <div class="form-row">
                        <label for="inputEmail4">Full Name</label>
                        <input type="text" class="form-control" value="{{ $customer-> first_name.' '.$customer-> first_name}}" name="full_name" placeholder="First Name">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress">Email</label>
                      <input type="email" class="form-control" value="{{ $customer-> email_address}}" name="email_address" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Phone Number</label>
                      <input type="number" class="form-control" value="{{ $customer-> phone_number}}" name="phone_number" placeholder="Phone Number">
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Address Area</label>
                      <textarea class="form-control" name="address" rows="3">{{ $customer-> address}}</textarea>
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Save saving info</button>
                    {{Form::close()}}
                  </div>
          </div>
        </div>
@endsection

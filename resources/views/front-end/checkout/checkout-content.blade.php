@extends('front-end.master')
@section('body')
  <!-- ============================================
    Search box and main body start
  ============================================ -->
  <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="message">
                <h4 class="text-center text-success">You have to login complete you valuable order. If you are not register then please register than please register first. </h4>
              </div>
            </div>
            <div class="col-md-6">

                {{Form::open(['route' =>'customer-sign-up', 'method' =>'POST', 'class'=>'form-open'])}}
                 <h3>Register if you are not registed before</h3>
                 <hr>
                    <div class="form-row">
                        <label for="inputEmail4">First Name</label>
                        <input type="text" class="form-control" name="first_name" placeholder="First Name">

                    </div>
                    <div class="form-row">
                        <label for="inputPassword4">Last Name</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress">Email</label>
                      <input type="email" class="form-control" name="email_address" placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Password</label>
                      <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Phone Number</label>
                      <input type="number" class="form-control" name="phone_number" placeholder="Phone Number">
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Example textarea</label>
                      <textarea class="form-control" name="address" rows="3"></textarea>
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Register</button>
                    {{Form::close()}}
                  </div>




            <div class="col-md-6">
              {{Form::open(['route' =>'customer-login', 'method' =>'POST','class'=>'form-open'])}}
                 <h3 style="text-align:center">Please Login</h3>
                 <br>
                 <h4 class="text-center text-danger">{{Session::get('message')}}</h4>
                 <hr>
                    <div class="form-group">
                      <label for="inputAddress">Email</label>
                      <input type="email" class="form-control" name="email_address"  placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Password</label>
                      <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Register</button>
                    {{Form::close()}}
            </div>
          </div>
        </div>
@endsection

@extends('front-end.master')
@section('body')
  <!-- ============================================
    Search box and main body start
  ============================================ -->
  <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="message">
                <h5 class="text-center">Dear {{ Session::get('customerName') }}. You have to give us product payment method....</h5>
              </div>
            </div>

            <div class="col-md-8 col-md-offset-2">
             {{ Form::open(['route'=>'new-order','method'=>'POST','class'=>'form-open'])}}
               <table class="table table-bordered">
                 <tr>
                   <th>Cash On Delivery</th>
                   <td><input type="radio" name="payment_type" value="Cash">Cash On Delivery</td>
                 </tr>
                 <tr>
                   <th>Paypal</th>
                   <td><input type="radio" name="payment_type" value="Paypal">Paypal</td>
                 </tr>
                 <tr>
                   <th>Master Cart</th>
                   <td><input type="radio" name="payment_type" value="Mastercart">Master Cart</td>
                 </tr>
                 <tr>
                   <th></th>
                   <td><input type="submit" name="btn" value="confirm order"></td>
                 </tr>
               </table>
            {{Form::close()}}
          </div>
          </div>
        </div>
@endsection

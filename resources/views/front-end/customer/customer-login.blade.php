@extends('front-end.master')
@section('body')
  <!-- ============================================
    Search box and main body start
  ============================================ -->
  <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="message">

              </div>
            </div>
            <div class="col-md-6">

                {{Form::open(['route' =>'customer-sign-up', 'method' =>'POST', 'class'=>'form-open'])}}
                 <h3>Register Heare</h3>
                 <hr>
                    <div class="form-row">
                        <label for="inputEmail4">First Name</label>
                        <input type="text" class="form-control" name="first_name" placeholder="First Name" required>
                        <span class="text-danger">{{ $errors -> has('first_name') ? $errors -> first('first_name') : ' ' }}</span>

                    </div>
                    <div class="form-row">
                        <label for="inputPassword4">Last Name</label>
                        <input type="text" class="form-control" name="last_name" placeholder="Last Name" required>
                        <span class="text-danger">{{ $errors -> has('last_name') ? $errors -> first('last_name') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                      <label for="inputAddress">Email</label>
                      <input type="email" class="form-control" name="email_address" placeholder="Email" required>
                      <span class="text-danger">{{ $errors -> has('email_address') ? $errors -> first('email_address') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Password</label>
                      <input type="password" class="form-control" name="password" placeholder="Password" required>
                      <span class="text-danger">{{ $errors -> has('password') ? $errors -> first('password') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Phone Number</label>
                      <input type="number" class="form-control" name="phone_number" placeholder="Phone Number" required>
                      <span class="text-danger">{{ $errors -> has('phone_number') ? $errors -> first('phone_number') : ' ' }}</span>
                    </div>
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Example textarea</label>
                      <textarea class="form-control" name="address" rows="3" required></textarea>
                      <span class="text-danger">{{ $errors -> has('address') ? $errors -> first('address') : ' ' }}</span>
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Register</button>
                    {{Form::close()}}
                  </div>




            <div class="col-md-6">
              {{Form::open(['route' =>'customer-login', 'method' =>'POST','class'=>'form-open'])}}
                 <h3 style="text-align:center">Please Login</h3>
                 <br>
                 <h4 class="text-center text-danger">{{Session::get('message')}}</h4>
                 <hr>
                    <div class="form-group">
                      <label for="inputAddress">Email</label>
                      <input type="email" class="form-control" name="email_address"  placeholder="Email">
                    </div>
                    <div class="form-group">
                      <label for="inputAddress2">Password</label>
                      <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <button type="submit" name="btn" class="btn btn-primary">Register</button>
                    {{Form::close()}}
            </div>
          </div>
        </div>
@endsection

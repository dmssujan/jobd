@extends('front-end.master')
@section('body')
  <!-- ============================================
  	Search box and main body start
  ============================================ -->
  {{-- <div class="main-body">
    <div class="main-search-box">
    	 <div class="contanier">
         <div class="row">
           <div class="col-sm-12 col-md-12">
           	 <div class="main-heading text-center">
                 <h4>Amazon Price Tracker, View Price History and Start Tracking Now!</h4>
           	 </div>
                <div class="search-box">
                   <form class="example" action="#">
                     <input type="text" placeholder="Enter Product Amazon ASIN, Title or URL" name="search">
                     <button type="submit">Go</button>
                  </form>
                </div>
           </div>
         </div>
    	 </div>

    </div>
  </div> --}}
  <!-- ==================================
  category show
  ================================== -->
  <div class="category-show">
   <div class="contanier">
    <div class="row">
    @foreach ($categories as $category)
     <div class="col-md-3">
     	  <div class="category-list">
          <ul>
          	<li><a href="{{ route('category-product',['id'=>$category->id]) }}"><img src="{{ asset('/') }}/front-end/assets/images/appliances.svg" alt=""> {{ $category->category_name}}</a></li>
          </ul>
     	  </div>
     </div>
     @endforeach
    </div>
   </div>
  </div>
  <!-- ===========================
  Category product show
  =========================== -->
  <div class="popular-product">
     <h4 class="text-center">Popular Category</h4>
     <hr />
    <div class="contanier">
       <div class="row">
          @foreach ( $newProducts as $newProduct)
            <div class="col-md-3 col-sm-3">
              <div class="product-image">
                   <img src="{{ asset($newProduct ->product_image ) }}" alt="Avatar" class="image">
                     <div class="overlay">
                      <div class="text">
                         <ul>
                           <li><a href="">demo</a></li>
                           <li><a href="{{ route('product-details', ['id' => $newProduct-> id, 'name'=>$newProduct->product_name] )}}">Name: {{ $newProduct -> product_name}}</a></li>
                           <li>price: ${{ $newProduct -> product_price}}</li>
                           <li style="margin-top:15px;"><a href="{{ route('product-details',['id' =>$newProduct->id,'name'=>$newProduct->product_name ])}}" class="btn btn-secondary text-center">view product</a></li>
                        </ul>
                      </div>
                    </div>
                 </div>
              </div>
               <hr />
          @endforeach
          </div>
       </div>
    </div>
  </div>
  <!--  ========================================
   Best review product
   ======================================== -->
   <div class="popular-product">
     <h4 class="text-center">Best review product</h4>
     <hr />
    <div class="contanier">
       <div class="row">
          <div class="col-md-3 col-sm-3">
            <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>

            </div>
            <div class="col-md-3 col-sm-3">
               <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3">

              <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
          </div>
       </div>
    </div>
  </div>
  <!-- ============================================
   Sessional product
  ============================================ -->
  <div class="popular-product">
     <h4 class="text-center">Sessional product</h4>
     <hr />
    <div class="contanier">
       <div class="row">
          <div class="col-md-3 col-sm-3">
            <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>

            </div>
            <div class="col-md-3 col-sm-3">
               <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3">

              <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
  			           	<li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
          </div>
       </div>
    </div>
  </div>
  <!-- ================================
  Footer part
  ================================ -->

@endsection

@extends('front-end.master')
@section('body')
<!-- =====================================
  More Category products
  New Home page
  ===================================== -->
  <div class="newHomepage">
    <div class="homepageNew">
        <div class="homePageless">
         <div class="contanier">
           <div class="row">
             <div class="col-md-3">
               <div class="categoryWH">
                 <h5>Category Name</h5>
               </div>
               <div class="ul-try">
                 <ul>
                   @foreach ($categories as $category)
                     <li><a href="{{route('/')}}">{{ $category->category_name}}</a></li>
                   @endforeach


                 </ul>
               </div>
             </div>
              <div class="col-md-3">
                <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo">
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>

                 <a href="#" class="btn btn-secondary text-center">view product</a>
              </div>
              </div>
              <div class="col-md-3">
                 <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo">
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>

                 <a href="#" class="btn btn-secondary text-center">view product</a>
              </div>

              </div>
              <div class="col-md-3">
                <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo">
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>

                 <a href="#" class="btn btn-secondary text-center">view product</a>
              </div>
              </div>
           </div>
         </div>

        </div>
        <!-- New Line start -->
        <div class="homePageless">
         <div class="contanier">
           <div class="row">
             <div class="col-md-3">
               <div class="categoryWH">
                 <h5>Category Name One</h5>
               </div>
               <div class="ul-try">
                 <ul>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Holiday Deals</a></li>
                   <li><a href="#">Deals in electronics</a></li>
                   <li><a href="#">Stream Music</a></li>
                 </ul>
               </div>
             </div>
              <div class="col-md-3">
                <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo">
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>

                 <a href="#" class="btn btn-secondary text-center">view product</a>
              </div>
              </div>
              <div class="col-md-3">
                 <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo">
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>

                 <a href="#" class="btn btn-secondary text-center">view product</a>
              </div>

              </div>
              <div class="col-md-3">
                <div class="product-image">
                 <img src="{{ asset('/') }}/front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo">
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>

                 <a href="#" class="btn btn-secondary text-center">view product</a>
              </div>
              </div>
           </div>
         </div>

        </div>
        <!-- New Line end -->
        <!-- Another Line start -->
        <div class="homePageless">
         <div class="contanier">
           <div class="row">
             <div class="col-md-3">
               <div class="categoryWH">
                 <h5>Latest Product</h5>
               </div>
               <div class="ul-try">
                 <ul>
                   @foreach ($newProducts as $newProduct)
                    <li><a href="#">{{$newProduct ->product_name }}</a></li>
                   @endforeach

                 </ul>
               </div>
             </div>
             @foreach ($newProducts as $newProduct)
               <div class="col-md-3">
                 <div class="product-image">
                  <img src="{{ asset($newProduct->product_image) }}" alt="Avatar" class="image">
                    <div class="overlay">
                     <div class="text">
                        <ul>
                     <li><a href="#">Amazon price:$20</a></li>
                     <li><a href="#">ebay price:$30</a></li>
                     <li><a href="#">our price: ${{ $newProduct->product_price }}</a></li>
                     <li><a href="#">many company:$50</a></li>
                       </ul>
                     </div>
                   </div>
               </div>
               <div class="category-productinfo">
                 <p>{{ $newProduct->short_description}}</p>

                  <a href="#" class="btn btn-secondary text-center">view product</a>
               </div>
               </div>
             @endforeach



           </div>
         </div>

        </div>
        <!-- Another line end -->

    </div>
  </div>


@endsection

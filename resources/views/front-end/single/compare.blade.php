@extends('front-end.master')

@section('body')
<div class="compare-main-page"> 
    <div class="contanier"> 
       <div class="row"> 
         <div class="col-md-12"> 
           <div class="com-an-mar"> 
              <h5>Compare to another market place</h5>
           </div>
           <div class="compare-table"> 
              <table class="table table-bordered">
   <thead>
     <tr>
       <th scope="col">Compare</th>
       <th scope="col">Amazon</th>
       <th scope="col">ebay</th>
       <th scope="col">aliexpress</th>
     </tr>
   </thead>
   <tbody>
     <tr>
       <th scope="row">price</th>
       <td>$50</td>
       <td>$80</td>
       <td>$120</td>
     </tr>
     <tr>
       <th scope="row">shipping</th>
       <td>Free</td>
       <td>$12</td>
       <td>$5</td>
     </tr>
     <tr>
       <th scope="row">discount</th>
       <td>5%</td>
       <td>8%</td>
       <td>10%</td>
     </tr>
    
   </tbody>
 </table>
           </div>
         </div>
       </div>
    </div>
 </div>
@endsection
@extends('front-end.master')
@section('body')
<!-- ============================================
    Search box and main body start
  ============================================ -->
  <div class="single-product">
    <div class="contanier">
       <div class="row">
       <div class="col-md-5 col-sm-6">
         <div class="single-image">
           <img id="img_01" src="{{ asset('/') }}front-end/assets/images/p2.jpg" data-zoom-image="{{ asset('/') }}front-end/assets/images/p1.jpg">
          </div>

       </div>
       <div class="col-md-7 col-sm-6">
         <div class="product-heading">
              <h4>Coverking Front Custom Fit Floor Mats for Select Truck Models - 40 Oz Carpet (Gray)</h4>
         </div>
         <div class="product-info">
          <ul>
            <li>Brand:<a href="#">amazon</a></li>
            <li>Last Prize:$40</li>
            <li>Category Name:<a href="#">Appliances</a></li>
            <li>our Price:$25</li>
            <li>View same product:<a href="#">amazon</a></li>
            <li>View same product:<a href="#">ebay</a></li>
            <li>View same product:<a href="#">aliexpress</a></li>
          </ul>
          <div class="add-card">
          <button type="button" class="btn btn-success">Add to card</button>
          </div>
         </div>
         <div class="product-compare">
           <h5>product compare</h5>
            <table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">Compare</th>
        <th scope="col">Amazon</th>
        <th scope="col">ebay</th>
        <th scope="col">aliexpress</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <th scope="row">price</th>
        <td>$50</td>
        <td>$80</td>
        <td>$120</td>
      </tr>
      <tr>
        <th scope="row">shipping</th>
        <td>Free</td>
        <td>$12</td>
        <td>$5</td>
      </tr>
      <tr>
        <th scope="row">discount</th>
        <td>5%</td>
        <td>8%</td>
        <td>10%</td>
      </tr>

    </tbody>
  </table>
         </div>
       </div>
       </div>
    </div>
  </div>
  <!-- =====================================
   Single Product description
  ===================================== -->
  <div class="single-product-des">
   <div class="row justify-content-md-center">
     <div class="col-md-8 col-sm-8 text-center">
       <h4 class="price-history">Price history</h4>
       <div class="side-redus">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
    <li class="nav-item">
      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Price history</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Price status</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Drops History</a>
    </li>
  </ul>
  <div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, incidunt qui fuga quam aliquam error! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor similique, molestias animi neque esse impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, incidunt qui fuga quam aliquam error! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor similique, molestias animi neque esse impedit!</div>
    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab"> ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, incidunt qui fuga quam aliquam error! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor similique, molestias animi neque esse impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, incidunt qui fuga quam aliquam error! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor similique, molestias animi neque esse impedit!</div>
    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab"> dolor sit amet, consectetur adipisicing elit. Quisquam, incidunt qui fuga quam aliquam error! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor similique, molestias animi neque esse impedit!Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, incidunt qui fuga quam aliquam error! Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor similique, molestias animi neque esse impedit!</div>
  </div>
     </div>
     </div>
   </div>
  </div>
  <!-- ========================================
  product image and description
  ======================================== -->
  <div class="product-de-image">
     <h4 class="text-center">More About Product</h4>
     <hr />
    <div class="row">
       <div class="col-md-6">
         <div class="big-image">
          <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="">
         </div>
       </div>
       <div class="col-md-6">
         <div class="image-desc">
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vitae iure inventore earum ea sequi mollitia distinctio tempora, nobis iste voluptate maxime quia fugit vel dicta fugiat rem quas porro eaque! Dignissimos numquam, eius consequatur quia tempora a provident in dolorum hic, eveniet nihil consequuntur ut, eum? Dolores laboriosam quis modi rerum quae, voluptate ullam, iusto at, distinctio neque vitae quasi voluptates aliquam suscipit nisi magni cumque dolorem harum itaque nesciunt hic! Inventore possimus repudiandae sed quae, eligendi magni error, quas consectetur asperiores eius aliquid odio, dicta autem! Placeat sapiente sunt fugit aut culpa esse, quia maiores recusandae impedit modi. Consequuntur soluta distinctio eius. Maxime quaerat quidem impedit laudantium nemo natus blanditiis magni sed aliquam sapiente dignissimos architecto doloribus voluptates porro earum, sit expedita cum mollitia temporibus voluptatem quas, labore. Expedita rem aliquam id nobis magni dolorem saepe sunt delectus eos, aperiam corrupti, eveniet maxime fugiat excepturi ducimus. Repellat, temporibus iusto.</p>
         </div>
       </div>
    </div>
  </div>

  <!-- ======================================
  related products
  ====================================== -->
   <div class="popular-product">
     <h4 class="text-center">Related products</h4>
     <hr />
    <div class="contanier">
       <div class="row">
          <div class="col-md-3 col-sm-3">
            <div class="product-image">
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>

            </div>
            <div class="col-md-3 col-sm-3">
               <div class="product-image">
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3">

              <div class="product-image">
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3 col-sm-3">
              <div class="product-image">
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                    <li><a href="#">demo</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
            </div>
          </div>
       </div>
       {{-- ====== --}}
       <div class="contanier">
          <div class="row">
             <div class="col-md-3 col-sm-3">
               <div class="product-image">
                    <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                      <div class="overlay">
                       <div class="text">
                          <ul>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                         </ul>
                       </div>
                     </div>
                  </div>

               </div>
               <div class="col-md-3 col-sm-3">
                  <div class="product-image">
                    <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                      <div class="overlay">
                       <div class="text">
                          <ul>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                         </ul>
                       </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3">

                 <div class="product-image">
                    <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                      <div class="overlay">
                       <div class="text">
                          <ul>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                         </ul>
                       </div>
                     </div>
                  </div>
               </div>
               <div class="col-md-3 col-sm-3">
                 <div class="product-image">
                    <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                      <div class="overlay">
                       <div class="text">
                          <ul>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                       <li><a href="#">demo</a></li>
                         </ul>
                       </div>
                     </div>
                  </div>
               </div>
             </div>
          </div>
          {{-- ============== --}}
          <div class="contanier">
             <div class="row">
                <div class="col-md-3 col-sm-3">
                  <div class="product-image">
                       <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                         <div class="overlay">
                          <div class="text">
                             <ul>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                            </ul>
                          </div>
                        </div>
                     </div>

                  </div>
                  <div class="col-md-3 col-sm-3">
                     <div class="product-image">
                       <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                         <div class="overlay">
                          <div class="text">
                             <ul>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                            </ul>
                          </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3 col-sm-3">

                    <div class="product-image">
                       <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                         <div class="overlay">
                          <div class="text">
                             <ul>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                            </ul>
                          </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-3 col-sm-3">
                    <div class="product-image">
                       <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                         <div class="overlay">
                          <div class="text">
                             <ul>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                          <li><a href="#">demo</a></li>
                            </ul>
                          </div>
                        </div>
                     </div>
                  </div>
                </div>
             </div>
    </div>
  </div>


@endsection

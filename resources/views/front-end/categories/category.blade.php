@extends('front-end.master')

@section('body')
<div class="more-products"> 
    <div class="row"> 
      <div class="col-md-3 col-sm-6">
         <div class="manu-heading"> 
            <h6 class="text-center">Top Manufacturers:</h6>
         </div>
         <div class="manufactures-list"> 
           <ul>
             <li><a href="#">demo one</a></li>
             <li><a href="#">demo two</a></li>
             <li><a href="#">demo three</a></li>
             <li><a href="#">demo four</a></li>
             <li><a href="#">demo five</a></li>
             <li><a href="#">demo six</a></li>
           </ul>
         </div>
      </div>
      <div class="col-md-9 col-sm-6"> 
        <div class="category-9"> 
        <div class="row"> 
           <div class="col-md-4"> 
             
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
           </div>
           <div class="col-md-4">
             
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
           </div>
           <div class="col-md-4">
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
             </div>
             
  
        </div>
  
       </div> 
        <!--  new line start -->
        <div class="category-9"> 
        <div class="row"> 
           <div class="col-md-4"> 
             
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
           </div>
           <div class="col-md-4">
             
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
           </div>
           <div class="col-md-4">
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
             </div>
             
  
        </div>
  
       </div>
            <!-- end line -->
            <!-- new loop start -->
            <!--  new line start -->
        <div class="category-9"> 
        <div class="row"> 
           <div class="col-md-4"> 
             
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
              </div>
              <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
           </div>
           <div class="col-md-4">
             
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
           </div>
           <div class="col-md-4">
              <div class="product-image"> 
                 <img src="{{ asset('/') }}front-end/assets/images/p2.jpg" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li><a href="#">our price:$40</a></li>
                    <li><a href="#">many company:$50</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
               <div class="category-productinfo"> 
                <p>Ice-O-Matic IOD150 150 Lb Capacity 22" Wide Ice Dispenser W/ Bin Kit KBT15022</p>
                <a class="text-center" href="#">view product</a>   
              </div>
             </div>
             
  
        </div>
  
       </div>
            <!-- end line -->
            <!-- new loop end -->
      </div>
    </div>
  </div>
  
@endsection
@extends('front-end.master')
@section('body')
<!-- =====================================
  More Category products
  New Home page
  ===================================== -->
  <div class="more-products">
    <div class="row">
      <div class="col-md-3 col-sm-6">
         <div class="manu-heading">
            <h6 class="text-center">Top Manufacturers:</h6>
         </div>
         <div class="manufactures-list">
           <ul>
             <li><a href="#">demo one</a></li>
             <li><a href="#">demo two</a></li>
             <li><a href="#">demo three</a></li>
             <li><a href="#">demo four</a></li>
             <li><a href="#">demo five</a></li>
             <li><a href="#">demo six</a></li>
           </ul>
         </div>
      </div>
      <div class="col-md-9 col-sm-6">
        <div class="category-9">
        <div class="row">
          @foreach ($categoryProducts as $categoryProduct)
           <div class="col-md-4">

              <div class="product-image">
                 <img src="{{ asset($categoryProduct -> product_image) }}" alt="Avatar" class="image">
                   <div class="overlay">
                    <div class="text">
                       <ul>
                    <li><a href="#">Amazon price:$20</a></li>
                    <li><a href="#">ebay price:$30</a></li>
                    <li>our price:$ {{ $categoryProduct ->product_price }}</li>
                    <li><a href="{{route('product-details', ['id' =>$categoryProduct->id, 'name'=>$categoryProduct->product_name ])}}">{{$categoryProduct -> product_name}}</a></li>
                      </ul>
                    </div>
                  </div>
               </div>
              <div class="category-productinfo">
                <p class="text-center">{{ $categoryProduct ->short_description }}</p>
                <a class="text-center btn btn-success" href="{{route('product-details', ['id' =>$categoryProduct->id, 'name'=>$categoryProduct->product_name ])}}" role="button">view product</a>
              </div>
           </div>
         @endforeach
        </div>

       </div>
        <!--  new line start -->

            <!-- end line -->
            <!-- new loop start -->
            <!--  new line start -->
            <!-- end line -->
            <!-- new loop end -->
      </div>
    </div>
  </div>
@endsection

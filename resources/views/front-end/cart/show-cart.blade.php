@extends('front-end.master')
@section('body')
  <!-- ============================================
    Search box and main body start
  ============================================ -->
  <div class="container">
          <div class="row">
            <div class="col-md-12 col-md-offset-0">
              <h3 class="text-center text-success">My Shopping Cart</h3>
              <br>
              <table class="table table-bordered">
                 <tr class="bg-primary text-center" style="color:#FFFFFF;">
                   <th>SL No</th>
                   <th>Name</th>
                   <th>Image</th>
                   <th>Price</th>
                   <th>update Quantity</th>
                   <th>Quantity</th>
                   <th>Total Price</th>
                   <th>Action</th>
                 </tr>
                 @php
                   $i = 1;
                   $sum = 0;
                 @endphp
                 @foreach ($cartProducts as $cartProduct )
                   <tr class="text-center">
                     <td>{{ $i++}}</td>
                     <td>{{$cartProduct ->name}}</td>
                     <td><img src="{{asset($cartProduct ->options->image)}}" alt="" height="50" width="50"></td>
                     <td>{{$cartProduct ->price}}</td>
                     <td>
                       {{ Form::open(['route' =>'update-cart', 'method'=>'post'])}}
                         <input type="number" name="qty" value="{{ $cartProduct-> qty}}" min="1"  />
                         <input type="hidden" name="rowId" value="{{ $cartProduct-> rowId}}" min="1"  />
                         <input type="submit" name="btn" value="update" />
                       {{ Form::close()}}
                    </td>
                     <td>{{$cartProduct ->qty}}</td>
                     <td>{{ $total = $cartProduct ->price * $cartProduct ->qty}}</td>

                     <td>
                       <a href="{{ route('delete-cart-item', ['rowId' =>$cartProduct->rowId ])}}" class="btn btn-danger btn-xs" title="Delete">
                         <i class="fa fa-trash"  aria-hidden="true"></i>
                       </a>
                     </td>
                   </tr>
                   @php
                     $sum = $total + $sum;
                   @endphp
                 @endforeach

              </table>
              <table class="table table-bordered">
                <tr>
                  <th>Item Total Price</th>
                  <td>{{$sum}}</td>
                </tr>
                <tr>
                  <th>Vat Total</th>
                  <td>{{  $vat = 0 }}</td>
                </tr>
                <tr>
                  <th>Grand Total</th>
                  <td>{{ $orderTotal = $sum + $vat }}</td>
                  @php
                    Session::put('orderTotal', $orderTotal);
                  @endphp
                </tr>
              </table>
            </div>
          </div>
          <div class="row">
             <div class="col-md-12 col-md-offset-0">

                 <a href="{{route('home2')}}" class="btn btn-success ">Contunue Shipping</a>
                 @if (Session::get('customerId') && Session::get('shippingId'))

                   <a href="{{route('checkout-payment')}}" class="btn btn-success pull-right">Checkout</a>

                 @elseif(Session::get('customerId'))
                   <a href="{{route('checkout-shipping')}}" class="btn btn-success pull-right">Checkout</a>
                 @else
                    <a href="{{route('checkout')}}" class="btn btn-success pull-right">Checkout</a>
                 @endif

             </div>
          </div>
        </div>
@endsection

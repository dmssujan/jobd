<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
|---------------------------------------------------------------------------
|  Front End
|---------------------------------------------------------------------------
|
|
*/

// Route::get('/', function () {
//     return view('front-end.home.home');
// });
// Route::get('/',[
//   'uses' => 'NewShopController@index',
//   'as'   =>'/'
// ]);
// Route::get('/',[
//   'uses' => 'ScrapingController@index',
//   'as'   =>'/'
// ]);
Route::get('/search',[
    'uses' => 'NewShopController@search',
    'as'   =>'search'
]);
Route::get('/',[
    'uses' => 'NewShopController@home2',
    'as'   =>'home2'
]);
Route::get('/category-product/{id}',[
    'uses' => 'NewShopController@categoryProduct',
    'as'   =>'category-product'
]);
Route::get('/product-details/{id}/{name}',[
    'uses' => 'NewShopController@productDetails',
    'as'   =>'product-details'
]);
Route::post('/cart/add',[
    'uses' => 'CartController@addTocart',
    'as'   =>'add-to-cart'
]);
Route::get('/cart/show',[
    'uses' => 'CartController@showCart',
    'as'   =>'show-cart'
]);
Route::get('/cart/delete/{id}',[
    'uses' => 'CartController@deleteCart',
    'as'   =>'delete-cart-item'
]);
Route::post('/cart/update',[
    'uses' => 'CartController@updateCart',
    'as'   =>'update-cart'
]);
Route::get('/checkout',[
    'uses' => 'CheckoutController@index',
    'as'   =>'checkout'

]);
Route::post('/customer/registration',[
    'uses' => 'CheckoutController@customerSignup',
    'as'   =>'customer-sign-up'
]);



Route::post('/checkout/customer-login',[
    'uses' => 'CheckoutController@checkoutCusLogin',
    'as'   =>'customer-login'
]);

Route::post('/checkout/customer-logout',[
    'uses' => 'CheckoutController@checkoutCusLogout',
    'as'   =>'customer-logout'
]);

Route::get('/checkout/new-customer-login',[
    'uses' => 'CheckoutController@newCusLogin',
    'as'   =>'new-customer-login'
]);




Route::get('/checkout/shipping',[
    'uses' => 'CheckoutController@shippingForm',
    'as'   =>'checkout-shipping'
]);

Route::post('/shipping/save',[
    'uses' => 'CheckoutController@saveShippingInfo',
    'as'   =>'new-shipping'
]);
Route::get('/checkout/payment',[
    'uses' => 'CheckoutController@paymentForm',
    'as'   =>'checkout-payment'
]);
Route::post('/checkout/order',[
    'uses' => 'CheckoutController@newOrder',
    'as'   =>'new-order'
]);
Route::get('/complete/order',[
    'uses' => 'CheckoutController@completeOrder',
    'as'   =>'complite-order'
]);


Route::get('/single', function() {
    return view('front-end.single.single');
});
Route::get('/compare', function() {
    return view('front-end.single.compare');
});


Route::get('/category', function() {
 return view('front-end.categories.category');
});


/*
|---------------------------------------------------------------------------
|  Back End
|---------------------------------------------------------------------------
|
|
*/
/* category start */
Route::group(['middleware' => ['djob']], function () {
 Route::get('/category/add', [
   'uses' => 'CategoryController@index',
   'as'   => 'add-category'
 ]);

 Route::get('/category/manage', [
   'uses' => 'CategoryController@manageCategory',
   'as'   => 'manage-category'
 ]);

Route::post('/category/save', [
   'uses' => 'CategoryController@saveCategory',
   'as'   => 'new-category'
 ]);
Route::get('/category/unpublished/{id}', [
   'uses' => 'CategoryController@unpublishedCategory',
   'as'   => 'unpublished-category'
 ]);
Route::get('/category/published/{id}', [
   'uses' => 'CategoryController@publishedCategory',
   'as'   => 'published-category'
 ]);

 Route::get('/category/edit/{id}', [
    'uses' => 'CategoryController@editCategory',
    'as'   => 'edit-category'
  ]);
Route::post('/category/update/', [
     'uses' => 'CategoryController@updateCategory',
     'as'   => 'update-category'
]);
Route::get('/category/delete/{id}', [
     'uses' => 'CategoryController@deleteCategory',
     'as'   => 'delete-category'
]);
/* Catagory end*/
/* Brand start */
Route::get('/brand/add', [
     'uses' => 'BrandController@index',
     'as'   => 'add-brand'
]);
Route::get('/brand/manage', [
     'uses' => 'BrandController@manageBrand',
     'as'   => 'manage-brand'
]);
Route::post('/brand/save', [
     'uses' => 'BrandController@saveBrand',
     'as'   => 'new-brand'
]);
Route::get('/brand/unpublish/{id}', [
     'uses' => 'BrandController@unpublishBrand',
     'as'   => 'unpublished-brand'
]);
Route::get('/brand/publish/{id}', [
     'uses' => 'BrandController@publishBrand',
     'as'   => 'published-brand'
]);
Route::get('/brand/edit/{id}', [
     'uses' => 'BrandController@editBrand',
     'as'   => 'edit-brand'
]);
Route::post('/brand/update', [
     'uses' => 'BrandController@updateBrand',
     'as'   => 'update-brand'
]);
Route::get('/brand/delete/{id}', [
     'uses' => 'BrandController@deleteBrand',
     'as'   => 'delete-brand'
]);
/* Brand end */
/* Product start */
Route::get('/product/add', [
     'uses' => 'ProductController@index',
     'as'   => 'add-product'
]);
Route::post('/product/save', [
     'uses' => 'ProductController@saveProduct',
     'as'   => 'new-product'
]);
Route::get('/product/manage', [
     'uses' => 'ProductController@manageProduct',
     'as'   => 'manage-product'
]);
Route::get('/product/unpublishe/{id}', [
     'uses' => 'ProductController@unpublishProduct',
     'as'   => 'unpublished-product'
]);
Route::get('/product/publishe/{id}', [
     'uses' => 'ProductController@publishProduct',
     'as'   => 'published-product'
]);
Route::get('/product/edit/{id}', [
     'uses' => 'ProductController@editProduct',
     'as'   => 'edit-product'
]);
Route::get('/product/delete/{id}', [
     'uses' => 'ProductController@deleteProduct',
     'as'   => 'delete-product'
]);
Route::post('/product/update/', [
     'uses' => 'ProductController@updateProduct',
     'as'   => 'update-product'
]);
Route::get('/order/manage-order', [
     'uses' => 'OrderController@manageOrderInfo',
     'as'   => 'manage-order',
     'middleware' => 'djob'
]);
Route::get('/order/view-order-details/{id}', [
     'uses' => 'OrderController@viewOrderDetails',
     'as'   => 'view-order-details'
]);
Route::get('/order/view-order-invoice/{id}', [
     'uses' => 'OrderController@viewOrderInvoice',
     'as'   => 'view-order-invoice'
]);
Route::get('/order/download-order-invoce/{id}', [
     'uses' => 'OrderController@downloadOrderInvoice',
     'as'   => 'download-order-invoce'
 ]);
Route::get('/order/edit-order/{id}', [
     'uses' => 'OrderController@editOrder',
     'as'   => 'edit-order'
 ]);
 Route::post('/order/update', [
      'uses' => 'OrderController@updateOrder',
      'as'   => 'update-order'
 ]);

 /* User Control*/
Route::get('/user/manage', [
     'uses' => 'UserController@manageUser',
     'as'   => 'manage-user'
 ]);
Route::get('/user/add', [
     'uses' => 'UserController@index',
     'as'   => 'add-user'
 ]);
Route::post('/logout', [
   'uses' => 'UserController@logout',
   'as'   => 'logout'
 ]);
});
/* Product end */




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
